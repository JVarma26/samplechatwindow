package com.example.chatapp;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter {

	private final Context context;

	private ArrayList<ListItem> listMessage;

	private class ViewHolder {
		private View leftView, rightView;
		private TextView mLeftTextViewName;
		private TextView mRightTextViewName;
		private ImageView mLeftImageView;
		private TextView mLeftTextViewMsg;
		private TextView mLeftTextViewTime;
		private ImageView mRightImageView;
		private TextView mRightTextViewMsg;
		private TextView mRightTextViewTime;

	}

	ViewHolder viewHolder = null;

	public ListAdapter(Context context, ArrayList<ListItem> listMessage) {
		this.context = context;
		this.listMessage = listMessage;

	}

	@Override
	public int getCount() {
		return listMessage.size();
	}

	@Override
	public ListItem getItem(int position) {
		return listMessage.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		// reuse Views
		if (rowView == null) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.list_view, null);

			viewHolder = new ViewHolder();
			// configure view holder
			// ViewHolder viewHolder = new ViewHolder();

			rowView.setTag(viewHolder);
			Log.d("TAG", "SETTAG");
		} else {
			viewHolder = (ViewHolder) rowView.getTag();

			Log.d("TAG", "GETTag");
		}

		viewHolder.mLeftTextViewMsg = (TextView) rowView
				.findViewById(R.id.list_box_left_text);
		viewHolder.mLeftTextViewTime = (TextView) rowView
				.findViewById(R.id.list_box_left_time);
		viewHolder.mLeftImageView = (ImageView) rowView
				.findViewById(R.id.list_box_left_icon);
		viewHolder.mLeftTextViewName = (TextView) rowView
				.findViewById(R.id.list_box_left_name);

		viewHolder.mRightTextViewMsg = (TextView) rowView
				.findViewById(R.id.list_box_right_text);
		viewHolder.mRightTextViewTime = (TextView) rowView
				.findViewById(R.id.list_box_right_time);
		viewHolder.mRightImageView = (ImageView) rowView
				.findViewById(R.id.list_box_right_icon);
		viewHolder.mRightTextViewName = (TextView) rowView
				.findViewById(R.id.list_box_right_name);

		viewHolder.leftView = rowView.findViewById(R.id.left_view);
		viewHolder.rightView = rowView.findViewById(R.id.right_view);

		// fill data

		String s = getItem(position).getname();

		Bitmap srcBitmapLocal = BitmapFactory.decodeResource(
				MainActivity.context.getResources(), getItem(position)
						.getIcon());
		//
		srcBitmapLocal = getCircleBitmap(srcBitmapLocal);

		if (s.equals("Jayesh")) {

			viewHolder.mLeftTextViewMsg.setText(s);
			viewHolder.mLeftTextViewTime.setText(getItem(position).getTime());
			viewHolder.mLeftTextViewName
					.setText(getItem(position).getMessage());

			viewHolder.rightView.setVisibility(View.GONE);
			viewHolder.leftView.setVisibility(View.VISIBLE);
			if (((BitmapDrawable) viewHolder.mRightImageView.getDrawable()) != null) {
				((BitmapDrawable) viewHolder.mRightImageView.getDrawable())
						.getBitmap().recycle();
			}
			// viewHolder.mLeftImageView.setImageResource(listMessage.get(position).getIcon());
			viewHolder.mLeftImageView.setImageDrawable(new BitmapDrawable(
					context.getResources(), srcBitmapLocal));
		} else {

			viewHolder.mRightTextViewMsg.setText(s);
			viewHolder.mRightTextViewTime.setText(getItem(position).getTime());
			viewHolder.mRightTextViewName.setText(getItem(position)
					.getMessage());

			viewHolder.rightView.setVisibility(View.VISIBLE);
			viewHolder.leftView.setVisibility(View.GONE);
			if (((BitmapDrawable) viewHolder.mLeftImageView.getDrawable()) != null) {
				((BitmapDrawable) viewHolder.mLeftImageView.getDrawable())
						.getBitmap().recycle();
			}
			// viewHolder.mLeftImageView.setImageResource(listMessage.get(position).getIcon());
			viewHolder.mRightImageView.setImageDrawable(new BitmapDrawable(
					context.getResources(), srcBitmapLocal));

		}

		return rowView;
	}

	private Bitmap getCircleBitmap(Bitmap bitmap) {

		final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		final Canvas canvas = new Canvas(output);

		final int color = Color.BLUE;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawOval(rectF, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		bitmap.recycle();

		return output;
	}

}