package com.example.chatapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {

	public static Context context;

	private String[] message = new String[] { "Hello", "Hi", "Hey" };

	private String[] name = new String[] { "Nishant", "Jayesh", "Pratik" };

	private int[] icon = new int[] { R.drawable.ic_home, R.drawable.ic_home,
			R.drawable.ic_home };

	Date date1 = new Date(System.currentTimeMillis());

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = getApplicationContext();
		final ListView mListView = (ListView) findViewById(R.id.list_view);

		ArrayList<ListItem> mListArrayItems = new ArrayList<ListItem>();

		Random r = new Random();
		int randomI;
		for (int i = 0; i < 40; i++) {
			randomI = r.nextInt(2);

			mListArrayItems.add(new ListItem(name[randomI], icon[randomI],
					date1 + "", message[randomI]));

		}

		ListAdapter adapter = new ListAdapter(getApplicationContext(),
				mListArrayItems);
		mListView.setAdapter(adapter);
	}
}