package com.example.chatapp;

public class ListItem {

	private String name;
	private String message;
	private String time;
	private int icon;

	public ListItem() {

	}

	public ListItem(String name, int icon, String time, String message) {

		this.name = name;
		this.icon = icon;
		this.time = time;
		this.message = message;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}


	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
